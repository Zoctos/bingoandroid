package com.fabien.pierre.projetandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ActivityGame extends AppCompatActivity {

    private Joueur joueur;
    private String mode;
    private int niveau;

    private GridView grid;
    private ArrayList<Integer> chiffreGrille;
    private ArrayList<Integer> caseFounded;
    private int nbAleatoire;
    private int nbCellValid;

    private ArrayList<String> texteGrille;
    private ArrayList<String> textFounded;
    private String textAleatoire;

    private BingoService bingoService;
    private BingoReceiver bingoReceiver;

    private boolean serviceActif;
    private boolean threadActif;
    private boolean isPlayerLose = false;
    private boolean isServiceNeeded;
    private int tirageMode;

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            BingoService.BingoBinder binder = (BingoService.BingoBinder) service;
            bingoService = binder.getService();

            bingoService.setModeTirage(tirageMode);
            serviceActif = true;
            if(bingoService != null)
                bingoService.reset();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceActif = false;
        }
    };

    private class BingoReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            //On récupère le tirage du service
            String tirage = intent.getStringExtra("tirage");
            if(tirage == null)
                return;

            //Si on a déjà remplit la grille
            if (chiffreGrille.size() == 9 || texteGrille.size() == 9) {

                //On vérifie que la valeur retourné n'indique pas l'échec du joueur
                if (tirage.equals("-1")) {
                    threadActif = false;

                    //Si on n'a pas déjà indiqué au joueur qu'il avait perdu on le fait
                    if (!isPlayerLose)
                        playerLose();

                } else {

                    //Si on doit tirer des nombres
                    if(tirageMode == Constants.TIRAGE_NUMBER) {

                        //On affiche la valeur
                        int n = Integer.parseInt(tirage);
                        nbAleatoire = n;
                        TextView textView = (TextView) findViewById(R.id.valueBingo);
                        textView.setText("" + n);

                    }
                    else if(tirageMode == Constants.TIRAGE_FILE){
                        //Si on doit tirer un mot on l'affiche
                        textAleatoire = tirage;
                        TextView textView = (TextView) findViewById(R.id.valueBingo);
                        textView.setText(tirage);
                    }
                    else if(tirageMode == Constants.TIRAGE_IMAGE){
                        //Si on doit tirer une image on la récupère et on l'affiche
                        int n = Integer.parseInt(tirage);
                        Bitmap image = BitmapFactory.decodeStream(getResources().openRawResource(n));
                        nbAleatoire = n;

                        ImageView imageView = (ImageView) findViewById(R.id.imageTirage);
                        imageView.setImageBitmap(image);

                        //Si l'aperçu de l'image est invisible on le rend visible
                        if(imageView.getVisibility() != View.VISIBLE)
                            imageView.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                //Sinon on doit remplir la grille

                if(tirageMode != Constants.TIRAGE_FILE) {
                    int n = Integer.parseInt(tirage);
                    if (!chiffreGrille.contains(n)) {
                        chiffreGrille.add(n);

                        //Si la grille est maintenant remplit on crée l'adapter et on l'affiche
                        if (chiffreGrille.size() == 9) {
                            GridAdapter gridAdapter = new GridAdapter(ActivityGame.this, tirageMode);
                            gridAdapter.setContentGrille(chiffreGrille);
                            grid.setAdapter(gridAdapter);
                        }
                    }
                }
                else{
                    if (!texteGrille.contains(tirage)) {
                        texteGrille.add(tirage);

                        //Si la grille est maintenant remplit on crée l'adapter et on l'affiche
                        if (texteGrille.size() == 9) {
                            GridAdapter adapter = new GridAdapter(ActivityGame.this, tirageMode);
                            adapter.setTexteGrille(texteGrille);
                            grid.setAdapter(adapter);
                        }
                    }
                }
            }

        }
    }

    @Override
    protected void onStart(){
        super.onStart();

        Intent intent = getIntent();

        //Si le service doit être actif
        if(intent.getBooleanExtra("serviceActif", true)) {
            Intent intentService = new Intent(this, BingoService.class);
            bindService(intentService, serviceConnection, Context.BIND_AUTO_CREATE);
            serviceActif = true;

        }
        else
            serviceActif = false;

        nbCellValid = 0;
        caseFounded = new ArrayList<Integer>();
        textFounded = new ArrayList<String>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        chiffreGrille = new ArrayList<Integer>();
        texteGrille = new ArrayList<String>();

        Intent intent = getIntent();

        //On récupère le joueur
        joueur = intent.getParcelableExtra("joueur");

        //On affiche le nombre de limcoins
        TextView textLimcoins = (TextView) findViewById(R.id.textGameLimcoins);
        textLimcoins.setText("Limcoins : "+joueur.getLimcoins());

        TextView textLevel = (TextView) findViewById(R.id.textGameLevel);

        grid = (GridView) findViewById(R.id.gridGame);

        //on récupère le mode (classique ou choix)
        mode = intent.getAction();
        isPlayerLose = false;

        //on récupère si on a besoin d'un service
        isServiceNeeded = intent.getBooleanExtra("serviceActif", true);

        //si on est en mode choix et qu'on a besoin d'un service ou si on est en mode classique
        if((mode.equals(Constants.MODE_CHOIX) && isServiceNeeded) || mode.equals(Constants.MODE_CLASSIC)){

            //on crée le service et le receiver
            bingoReceiver = new BingoReceiver();

            IntentFilter filter = new IntentFilter();
            filter.addAction("RANDOM");
            registerReceiver(bingoReceiver, filter);

            Intent intentService = new Intent(this, BingoService.class);
            intentService.setAction("ALEA");
            startService(intentService);

        }

        TextView textTirage = (TextView) findViewById(R.id.valueBingo);

        //On récupère le type de tirage
        tirageMode = intent.getIntExtra("modeTirage", Constants.TIRAGE_NUMBER);

        //Si on a besoin d'un service
        if(isServiceNeeded){

            textLevel.setText("Niveau 1");
            niveau = 1;

            if(tirageMode == Constants.TIRAGE_IMAGE) {
                textTirage.setVisibility(View.INVISIBLE);
                ImageView imageView = (ImageView) findViewById(R.id.imageTirage);
                imageView.setVisibility(View.VISIBLE);
            }

        }
        else{

            textLevel.setText("Niveau 0");
            niveau = 0;
            textTirage.setVisibility(View.INVISIBLE);

            //Si on veut tirer des valeurs d'un fichier on les récupère
            if(tirageMode == Constants.TIRAGE_FILE) {

                ArrayList<String> mots = new ArrayList<String>();

                try {

                    InputStream is = getResources().openRawResource(R.raw.android);
                    String texte = "";

                    int charactere;

                    while ((charactere = is.read()) != -1) {
                        texte += (char) charactere;
                    }

                    //On récupère chaque mot de chaque ligne
                    String[] lignes = texte.split("\n");
                    for (int i = 0; i < lignes.length; i++) {

                        String[] m = lignes[i].split(" ");
                        for (int e = 0; e < m.length; e++) {
                            mots.add(m[e]);
                            if (mots.size() == 9)
                                break;
                        }

                        if (mots.size() == 9)
                            break;
                    }

                    is.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                //On ajoute l'adapter
                GridAdapter adapter = new GridAdapter(ActivityGame.this, Constants.TIRAGE_FILE);
                adapter.setTexteGrille(mots);
                texteGrille = mots;
                grid.setAdapter(adapter);
            }
            else if(tirageMode == Constants.TIRAGE_IMAGE){

                //On récupère les images et on les ajoute a l'adapter et on l'affiche
                ArrayList<Integer> imagesId = new ArrayList<Integer>();
                imagesId.add(R.raw.yellow);
                imagesId.add(R.raw.black);
                imagesId.add(R.raw.green);
                imagesId.add(R.raw.rose);
                imagesId.add(R.raw.red);
                imagesId.add(R.raw.orange);
                imagesId.add(R.raw.gray);
                imagesId.add(R.raw.purple);
                imagesId.add(R.raw.cyan);

                GridAdapter adapter = new GridAdapter(ActivityGame.this, Constants.TIRAGE_IMAGE);
                adapter.setContentGrille(imagesId);
                chiffreGrille = imagesId;
                grid.setAdapter(adapter);

            }

        }

        //Si on a besoin d'un service on démarre le thread
        if(isServiceNeeded)
            startRandomService();

        //Si on clic sur une case de la GridView
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //Si les grilles sont complètes
                if(chiffreGrille.size() == 9 || texteGrille.size() == 9) {

                    //Si on doit tirer des images
                    if (tirageMode == Constants.TIRAGE_IMAGE) {

                        ImageView imageView = (ImageView) view;

                        //Si on a besoin d'un service
                        if (isServiceNeeded) {

                            //Si l'image tiré correspond à l'image sur laquelle on a cliqué
                            if (nbAleatoire == chiffreGrille.get(i)) {
                                if (!caseFounded.contains(i)) {
                                    imageView.setBackgroundColor(Color.BLUE);
                                    caseFounded.add(i);
                                    nbCellValid++;

                                    if (nbCellValid == 9) {
                                        resetGrille(true);
                                    }

                                }
                            } else {
                                //sinon le joueur a perdu
                                playerLose();
                            }

                        } else {
                            //Sinon on test pour savoir si le joueur a déjà cliqué ou non sur la case
                            if(!caseFounded.contains(i)) {
                                caseFounded.add(i);
                                imageView.setBackgroundColor(Color.BLUE);
                                nbCellValid++;

                                if (nbCellValid == 9) {
                                    playerWinWithoutService();
                                }
                            }
                        }

                    } else {
                        //Sinon on tire des nombres ou des mots

                        TextView valView = (TextView) view;

                        if (isServiceNeeded) {

                            boolean isGood = false;

                            //Si on tire des nombres et si la valeur correspond
                            if(tirageMode == Constants.TIRAGE_NUMBER && Integer.parseInt(valView.getText().toString()) == nbAleatoire){
                                isGood = true;
                                if(!caseFounded.contains(nbAleatoire)) {
                                    caseFounded.add(nbAleatoire);
                                    nbCellValid++;
                                    valView.setBackgroundColor(Color.BLUE);
                                    if (nbCellValid == 9) {
                                        resetGrille(true);
                                    }
                                }
                            } //Si on tire des mots et si la valeur correspond
                            else if(tirageMode == Constants.TIRAGE_FILE && valView.getText().toString().equals(textAleatoire)){
                                isGood = true;
                                if(!textFounded.contains(textAleatoire)) {
                                    textFounded.add(textAleatoire);
                                    nbCellValid++;
                                    valView.setBackgroundColor(Color.BLUE);
                                    if (nbCellValid == 9) {
                                        resetGrille(true);
                                    }
                                }
                            }

                            //Si on a perdu
                            if(!isGood){
                                playerLose();
                            }
                        } else {

                            //Sinon ça veut dire qu'on est en mode choix et qu'on tire des mots
                            String mot = valView.getText().toString();

                            if(!textFounded.contains(mot)) {
                                valView.setBackgroundColor(Color.BLUE);
                                textFounded.add(mot);
                                nbCellValid++;

                                if (nbCellValid == 9) {
                                    playerWinWithoutService();
                                }
                            }
                        }
                    }
                }

            }
        });

    }

    @Override
    protected void onStop(){
        super.onStop();

        //On indique qu'on écoute plus le service
        if(isServiceNeeded && serviceActif) {
            unbindService(serviceConnection);
            serviceActif = false;
            unregisterReceiver(bingoReceiver);
        }

        threadActif = false;

    }

    public void startRandomService(){

        //On crée un thread qui s'occupera de faire appel au service
        Thread threadRandom = new Thread(new Runnable() {
            @Override
            public void run() {
                threadActif = true;
                while(threadActif) {
                    if (bingoService != null) {

                        try {
                            bingoService.getRandomTirage();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(chiffreGrille.size() == 9 || texteGrille.size() == 9)
                            bingoService.setSendGrilleNumber(false);
                    }
                }
                bingoService.reset();
            }
        });

        threadRandom.start();
    }


    public void resetGrille(boolean isLevelUp){

        //Si le joueur doit passer au niveau suivant
        if(isLevelUp) {
            niveau++;
            joueur.setLimcoins(joueur.getLimcoins()+500);
            Toast.makeText(getApplicationContext(), "Bravo ! Vous passez au niveau suivant ! ", Toast.LENGTH_LONG).show();

        }

        //On réinitialise toutes les valeurs
        nbCellValid = 0;
        chiffreGrille = new ArrayList<Integer>();
        texteGrille = new ArrayList<String>();
        if(bingoService.getTimeWait() > 1000)
            bingoService.setTimeWait(bingoService.getTimeWait() - 500);
        bingoService.reset();
        caseFounded.clear();
        textFounded.clear();

        //On redéfinit les valeurs des champs
        TextView textNiveau = (TextView) findViewById(R.id.textGameLevel);
        textNiveau.setText("Niveau " + niveau);

        TextView textLimcoins = (TextView) findViewById(R.id.textGameLimcoins);
        textLimcoins.setText("Limcoins : "+joueur.getLimcoins());

        nbAleatoire = -1;
        textAleatoire = "-1";

        //Et on redéfinit le contenu du tirage
        if(tirageMode == Constants.TIRAGE_NUMBER || tirageMode == Constants.TIRAGE_FILE) {
            TextView textView = (TextView) findViewById(R.id.valueBingo);
            textView.setText("-1");
        }
        else if(tirageMode == Constants.TIRAGE_IMAGE){
            ImageView imageView = (ImageView) findViewById(R.id.imageTirage);
            imageView.setVisibility(View.INVISIBLE);
        }

    }

    public void playerLose(){

        //on indique que le joueur a perdu
        isPlayerLose = true;

        //si on est en en mode classique on propose à l'utilisateur d'acheter une nouvelle grille pour 250 limcoins
        if(joueur.getLimcoins() >= 250) {
            AlertDialog.Builder builderContinue = new AlertDialog.Builder(ActivityGame.this);
            builderContinue.setTitle("Perdu !");
            builderContinue.setMessage("Continuer avec une nouvelle grille pour 250 Limcoins ?")
                    .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                        //si il clique sur Oui on enlève 250 limcoins de son compte et on lui donne une nouvelle grille
                        public void onClick(DialogInterface dialog, int id) {
                            joueur.setLimcoins(joueur.getLimcoins()-250);
                            resetGrille(false);
                            isPlayerLose = false;
                        }
                    })
                    .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            playerCanRetry();
                        }
                    });
            //permet d'empêcher la fermeture de la boite en cliquant à côté
            builderContinue.setCancelable(false);

            builderContinue.create().show();
        }
        else{ //Sinon on propose au joueur de réessayer
            playerCanRetry();
        }

    }

    public void playerCanRetry(){

        //On propose au joueur de recommencer une partie
        AlertDialog.Builder builderRetry = new AlertDialog.Builder(ActivityGame.this);
        builderRetry.setTitle("Perdu !");
        builderRetry.setMessage("Recommencer une partie ?")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener(){
                    //sinon si il clique sur Non on lui propose de recommencer une partie du début
                    public void onClick(DialogInterface dialog, int id){

                        //On désactive le thread
                        threadActif = false;

                        //On ajoute le score que le joueur a fait
                        ScoreDAO scoreDAO = new ScoreDAO(ActivityGame.this);
                        scoreDAO.open();
                        scoreDAO.addScore(new Score(joueur, niveau));
                        scoreDAO.close();

                        //On passe les valeurs nécessaires à l'intent pour relancer l'Activity
                        Intent intentRetry = new Intent(ActivityGame.this, ActivityGame.class);
                        joueur.setLimcoins(500);
                        intentRetry.putExtra("joueur", joueur);
                        intentRetry.putExtra("serviceActif", isServiceNeeded);
                        intentRetry.putExtra("modeTirage", tirageMode);
                        intentRetry.setAction(mode);
                        startActivity(intentRetry);

                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener(){
                    //sinon on le renvoie à l'ActivityScore
                    public void onClick(DialogInterface dialog, int id){

                        //On désactive le thread
                        threadActif = false;

                        //On passe les valeurs nécessaires à l'intent pour passer au score
                        Intent intentScore = new Intent(ActivityGame.this, ActivityScore.class);
                        intentScore.putExtra("joueur", joueur);
                        intentScore.putExtra("level", niveau);
                        startActivity(intentScore);
                    }
                });
        //permet d'empêcher la fermeture de la boite en cliquant à côté
        builderRetry.setCancelable(false);
        builderRetry.create().show();

    }

    public void playerWinWithoutService(){

        //On rend visible le bouton de retour et on affiche un message
        Button buttonChoix = (Button) findViewById(R.id.buttonChoixReussi);
        buttonChoix.setVisibility(View.VISIBLE);

        Toast.makeText(ActivityGame.this, "Bravo ! Vous avez réussi !", Toast.LENGTH_LONG).show();

    }

    //Appelé par le bouton Retour
    public void onClickButtonChoixReussite(View view){

        //On retourne à l'écran de sélection du mode
        Intent intentAccueil = new Intent(ActivityGame.this, ActivityMode.class);
        intentAccueil.putExtra("joueur", joueur);
        startActivity(intentAccueil);

    }

}
