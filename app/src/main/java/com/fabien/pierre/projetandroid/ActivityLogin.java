package com.fabien.pierre.projetandroid;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class ActivityLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    //Appeler par le bouton Connexion
    public void connect(View view){

        //On crée un joueurDAO
        JoueurDAO joueurDAO = new JoueurDAO(this);
        joueurDAO.open();

        EditText editPseudo = (EditText) findViewById(R.id.editPseudo);
        EditText editPassword = (EditText) findViewById(R.id.editPassword);

        //On essaye de récupérer un joueur sur la base
        Joueur tConnect = joueurDAO.getJoueur(editPseudo.getText().toString(), editPassword.getText().toString());

        joueurDAO.close();

        //Si le joueur a indiqué les bons identifiants
        if(tConnect != null){
            Intent intent = new Intent(this, ActivityMode.class);
            intent.putExtra("joueur", tConnect);
            startActivity(intent);

        }
        else{ //Sinon on affiche un message d'erreur
            TextView textError = (TextView) findViewById(R.id.textErrorConnect);
            textError.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {

        //On ferme l'application
        finishAffinity();

    }

}
