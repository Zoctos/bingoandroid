package com.fabien.pierre.projetandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ActivityMode extends AppCompatActivity {

    private Joueur joueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);

        //On récupère le joueur
        Intent intent = getIntent();
        joueur = intent.getParcelableExtra("joueur");
        joueur.setLimcoins(500);
    }

    //Appelé par le bouton Classique
    public void modeClassic(View view){

        Intent intent = new Intent(this, ActivityGame.class);
        intent.setAction("CLASSIC");
        intent.putExtra("joueur", joueur);
        startActivity(intent);

    }

    //Appelé par le bouton Choix
    public void modeChoix(View view){

        Intent intent = new Intent(this, ActivityParametersChoix.class);
        intent.setAction("CHOIX");
        intent.putExtra("joueur", joueur);
        startActivity(intent);

    }

    //Appelé par le bouton Score
    public void affScore(View view){
        Intent intent = new Intent(this, ActivityScore.class);
        Integer level =-1;
        intent.putExtra("joueur", joueur);
        //intent.putExtra("level",level);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        //On retourne à l'ActivityLogin
        Intent intent = new Intent(this, ActivityLogin.class);
        startActivity(intent);

    }

}
