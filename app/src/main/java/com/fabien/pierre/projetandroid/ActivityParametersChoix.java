package com.fabien.pierre.projetandroid;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class ActivityParametersChoix extends AppCompatActivity {

    private Joueur joueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameters_choix);

        //On crée une liste
        Spinner spinnerMode = (Spinner) findViewById(R.id.spinnerMode);
        ArrayAdapter<CharSequence> arrayMode = ArrayAdapter.createFromResource(this, R.array.typeMode, android.R.layout.simple_spinner_item);
        arrayMode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMode.setAdapter(arrayMode);

        //On récupère le joueur
        Intent intent = getIntent();
        joueur = intent.getParcelableExtra("joueur");

    }

    //Appelé par le bouton Commencer
    public void onClickStartGame(View view){

        boolean isServiceActif = true;
        int mode = -1;

        Spinner spinnerMode = (Spinner) findViewById(R.id.spinnerMode);

        //On récupère la valeur de la liste
        if(spinnerMode.getSelectedItem().toString().equals("Fichier"))
            mode = Constants.TIRAGE_FILE;
        else if(spinnerMode.getSelectedItem().toString().equals("Images"))
            mode = Constants.TIRAGE_IMAGE;

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.groupRadioService);

        //On récupère la valeur indiquant si on a besoin d'un service
        if(radioGroup.getCheckedRadioButtonId() == R.id.radioServiceDesactive)
            isServiceActif = false;

        //On donne à l'intent les informations dont il a besoin
        Intent intent = new Intent(this, ActivityGame.class);
        intent.setAction("CHOIX");
        intent.putExtra("joueur", joueur);
        intent.putExtra("serviceActif", isServiceActif);
        intent.putExtra("modeTirage", mode);

        startActivity(intent);

    }

}
