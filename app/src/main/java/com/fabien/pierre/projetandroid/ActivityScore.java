package com.fabien.pierre.projetandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import javax.security.auth.login.LoginException;

public class ActivityScore extends AppCompatActivity {
    private GridView grid;
    private Joueur joueur;
    private int level;
    private ArrayList<Score> scores = new ArrayList<Score>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        //On crée un ScoreDAO
        ScoreDAO scoreDAO = new ScoreDAO(this);
        scoreDAO.open();

        grid = (GridView) findViewById(R.id.gridScore);

        //On récupère les informations nécessaires
        Intent intent = getIntent();
        joueur = intent.getParcelableExtra("joueur");
        level = intent.getIntExtra("level", -1);

        TextView textScore = (TextView) findViewById(R.id.textScore);

        //Si on était dans un mode qui rend possible l'ajout dans les scores
        if(level!=-1) {
            //On crée un nouveau score
            scoreDAO.addScore(new Score(joueur,level));
            textScore.setText("Score : "+level);
            textScore.setVisibility(View.VISIBLE);
        }
        else{
            textScore.setVisibility(View.INVISIBLE);
        }

        //On récupère la liste des scores et on les ajoute à la liste
        scores = scoreDAO.getAllScore();
        grid.setAdapter(new GridScoreAdapter(ActivityScore.this,scores));

        scoreDAO.close();
    }

    //Si on clique sur le bouton retour
    public void activityMode(View view){

        Intent intent = new Intent(this, ActivityMode.class);
        intent.putExtra("joueur", joueur);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, ActivityMode.class);
        intent.putExtra("joueur", joueur);
        startActivity(intent);

    }
}
