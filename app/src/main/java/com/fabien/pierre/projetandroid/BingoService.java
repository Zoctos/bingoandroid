package com.fabien.pierre.projetandroid;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Pierre on 05/12/2016.
 */

public class BingoService extends Service {

    private final IBinder binder = new BingoBinder();
    private final Random mGenerator = new Random();

    private boolean sendGrilleNumber = true;
    private int timeWait = 5000;
    private int modeTirage = Constants.TIRAGE_NUMBER;

    private ArrayList<Integer> imagesATires;

    private ArrayList<Integer> chiffreTires;
    private int nbMax = 100;

    private ArrayList<String> motsATires;

    public class BingoBinder extends Binder {
        BingoService getService(){
            return BingoService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public int getTimeWait(){
        return this.timeWait;
    }

    public void setSendGrilleNumber(boolean sendGrilleNumber){
        this.sendGrilleNumber = sendGrilleNumber;
    }

    public void setTimeWait(int timeWait){
        this.timeWait = timeWait;
    }

    public void setModeTirage(int modeTirage){
        this.modeTirage = modeTirage;
    }

    public void reset(){
        //Permet de réinitialiser les valeurs qu'on a récupéré
        if(chiffreTires != null)
            chiffreTires.clear();
        sendGrilleNumber = true;
    }

    public void getRandomTirage() throws InterruptedException, IOException {

        synchronized (this) {

            if(chiffreTires == null)
                chiffreTires = new ArrayList<Integer>();

            if(imagesATires == null)
                imagesATires = getAllImages();

            if(motsATires == null)
                motsATires = getAllWords();

            //Si on a atteint la fin des listes
            if(chiffreTires.size() == nbMax || chiffreTires.size() == imagesATires.size() || chiffreTires.size() == motsATires.size()){

                //On indique que le joueur a perdu
                Intent intentBroadcast = new Intent();
                intentBroadcast.putExtra("tirage", "-1");
                intentBroadcast.setAction("RANDOM");
                sendBroadcast(intentBroadcast);

            }
            else {

                int nbAlea = tirage();

                if (!sendGrilleNumber) {
                    while (chiffreTires.contains(nbAlea))
                        nbAlea = tirage();
                    chiffreTires.add(nbAlea);
                }

                Intent intentBroadcast = new Intent();

                if(modeTirage == Constants.TIRAGE_FILE) {
                    intentBroadcast.putExtra("tirage", motsATires.get(nbAlea));
                }
                else
                    intentBroadcast.putExtra("tirage", ""+nbAlea);

                intentBroadcast.setAction("RANDOM");
                sendBroadcast(intentBroadcast);

                //Si les valeurs tirés ne sont pas celles de la grille
                if (!sendGrilleNumber) {
                    wait(timeWait);
                }
            }
        }
    }

    private ArrayList<Integer> getAllImages(){

        //On récupère les images
        ArrayList<Integer> indices = new ArrayList<Integer>();

        indices.add(R.raw.black);
        indices.add(R.raw.blue);
        indices.add(R.raw.cyan);
        indices.add(R.raw.gray);
        indices.add(R.raw.green);
        indices.add(R.raw.orange);
        indices.add(R.raw.purple);
        indices.add(R.raw.red);
        indices.add(R.raw.rose);
        indices.add(R.raw.yellow);

        return indices;
    }

    private ArrayList<String> getAllWords() throws IOException {

        //On récupère les mots
        ArrayList<String> words = new ArrayList<String>();

        InputStream is = getResources().openRawResource(R.raw.fruits);
        String texte = "";

        int charactere;
        while((charactere = is.read()) != -1){
            texte += (char)charactere;
        }

        String[] lignes = texte.split("\n");
        for(int i = 0; i < lignes.length; i++){

            String[] mots = lignes[i].split(" ");
            for(int e = 0; e < mots.length; e++){
                words.add(mots[e]);
            }
        }

        is.close();

        return words;

    }

    private int tirage(){

        //En fonction de ce qu'on doit tirer le tirage est différent
        if(modeTirage == Constants.TIRAGE_NUMBER)
            return mGenerator.nextInt(nbMax) + 1;
        else if(modeTirage == Constants.TIRAGE_IMAGE) {
            int n = mGenerator.nextInt(imagesATires.size()-1);
            return imagesATires.get(n);
        }
        else{
            int n = mGenerator.nextInt(motsATires.size()-1);
            return n;
        }
    }

}
