package com.fabien.pierre.projetandroid;

/**
 * Created by Pierre on 07/12/2016.
 */

public class Constants {

    public static final String MODE_CLASSIC = "CLASSIC";
    public static final String MODE_CHOIX = "CHOIX";

    public static final int TIRAGE_NUMBER = 0;
    public static final int TIRAGE_FILE = 1;
    public static final int TIRAGE_IMAGE = 2;

}
