package com.fabien.pierre.projetandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Pierre on 18/12/2016.
 */

public abstract class DAOBase {

    protected final static int VERSION = 1;
    protected final static String NOM = "bingoDatabase.db";

    protected SQLiteDatabase mDB = null;
    protected DatabaseHandler mHandler = null;

    public DAOBase(Context mContext){
        this.mHandler = new DatabaseHandler(mContext, NOM, null, VERSION);
    }

    public SQLiteDatabase open(){
        mDB = mHandler.getWritableDatabase();
        return mDB;
    }

    public void close(){
        mDB.close();
    }

    public SQLiteDatabase getDB(){
        return mDB;
    }

}
