package com.fabien.pierre.projetandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Pierre on 14/12/2016.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    //TABLE JOUEUR
    private static final String JOUEUR_PSEUDO = "pseudo";
    private static final String JOUEUR_PASSWORD = "password";
    private static final String JOUEUR_TABLE = "Joueur";

    private static final String JOUEUR_TABLE_CREATE = "CREATE TABLE "+JOUEUR_TABLE+
            " ("+JOUEUR_PSEUDO+" TEXT PRIMARY KEY,"+
            JOUEUR_PASSWORD+" TEXT );";

    private static final String JOUEUR_TABLE_INSERT = "INSERT INTO "+JOUEUR_TABLE+
            " ("+JOUEUR_PSEUDO+", "+JOUEUR_PASSWORD+") VALUES ('testeur', 'test'), ('joueur', 'joue'), ('developpeur', 'developpe')";

    //TABLE SCORE
    private static final String SCORE_ID = "id";
    private static final String SCORE_JOUEUR = "joueurPseudo";
    private static final String SCORE_LEVEL = "level";
    private static final String SCORE_TABLE = "Score";

    private static final String SCORE_TABLE_CREATE = "CREATE TABLE "+SCORE_TABLE+
            " ("+SCORE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            SCORE_JOUEUR+" TEXT, "+
            SCORE_LEVEL+" INTEGER);";

    private static final String SCORE_TABLE_INSERT = "INSERT INTO "+SCORE_TABLE+
            " ("+SCORE_ID+", "+SCORE_JOUEUR+", "+SCORE_LEVEL+") VALUES "+
            "(1, 'developpeur', 1), (2, 'developpeur', 3), (3, 'testeur', 6), (4, 'testeur', 2)";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        //Si les tables existent on les supprime
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+JOUEUR_TABLE+";");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+SCORE_TABLE+";");

        //On recrée et on ajoute les valeurs de base
        sqLiteDatabase.execSQL(JOUEUR_TABLE_CREATE);
        sqLiteDatabase.execSQL(JOUEUR_TABLE_INSERT);

        sqLiteDatabase.execSQL(SCORE_TABLE_CREATE);
        sqLiteDatabase.execSQL(SCORE_TABLE_INSERT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
