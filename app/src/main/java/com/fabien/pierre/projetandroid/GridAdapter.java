package com.fabien.pierre.projetandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Pierre on 07/12/2016.
 */

public class GridAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Integer> contentGrille;
    private ArrayList<String> texteGrille;
    private int modeTirage;

    public GridAdapter(Context context, int modeTirage){
        this.context = context;
        this.contentGrille = new ArrayList<Integer>();
        this.texteGrille = new ArrayList<String>();
        this.modeTirage = modeTirage;
    }

    public void setContentGrille(ArrayList<Integer> contentGrille){
        this.contentGrille = contentGrille;
    }

    public void setTexteGrille(ArrayList<String> texteGrille){
        this.texteGrille = texteGrille;
    }

    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        //Si on doit afficher des chiffres ou des lettres on passe par un TextView
        if(modeTirage == Constants.TIRAGE_NUMBER || modeTirage == Constants.TIRAGE_FILE) {
            TextView text;

            if (view == null) {
                text = new TextView(context);
                text.setGravity(Gravity.CENTER);
            } else {
                text = (TextView) view;
            }

            if(modeTirage == Constants.TIRAGE_NUMBER)
                text.setText(contentGrille.get(i).toString());
            else
                text.setText(texteGrille.get(i));

            return text;
        } //Sinon on doit afficher des images et on passe par un ImageView
        else if(modeTirage == Constants.TIRAGE_IMAGE){
            ImageView image;

            if(view == null){
                image = new ImageView(context);
            }
            else{
                image = (ImageView) view;
            }

            image.setImageResource(contentGrille.get(i));

            return image;
        }

        return null;
    }


}
