package com.fabien.pierre.projetandroid;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Fabien on 13/12/2016.
 */

public class GridScoreAdapter  extends BaseAdapter {

    private Context context;
    private ArrayList<Score> contentGrille;

    public GridScoreAdapter(Context context, ArrayList<Score> contentGrille){
        this.context = context;
        this.contentGrille = contentGrille;
    }

    @Override
    public int getCount() {
        return contentGrille.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        TextView text;

        if(view == null){
            text = new TextView(context);
            text.setGravity(Gravity.CENTER);
            text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        }
        else{
            text = (TextView) view;
        }

        text.setText(contentGrille.get(i).toString());

        return text;
    }


}
