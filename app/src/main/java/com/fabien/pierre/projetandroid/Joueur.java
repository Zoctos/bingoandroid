package com.fabien.pierre.projetandroid;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Pierre on 05/12/2016.
 */

public class Joueur implements Parcelable {

    private String pseudo;
    private String password;
    private int limcoins;

    public Joueur(String pseudo, String password){
        this.pseudo = pseudo;
        this.password = password;
        this.limcoins = 0;
    }

    public Joueur(String pseudo, String password, int limcoins){
        this.pseudo = pseudo;
        this.password = password;
        this.limcoins = limcoins;
    }

    protected Joueur(Parcel in) {
        pseudo = in.readString();
        password = in.readString();
        limcoins = in.readInt();
    }

    public static final Creator<Joueur> CREATOR = new Creator<Joueur>() {
        @Override
        public Joueur createFromParcel(Parcel in) {
            return new Joueur(in);
        }

        @Override
        public Joueur[] newArray(int size) {
            return new Joueur[size];
        }
    };

    public String getPseudo(){
        return this.pseudo;
    }

    public String getPassword(){
        return this.password;
    }

    public int getLimcoins(){
        return this.limcoins;
    }

    public void setPseudo(String pseudo){
        this.pseudo = pseudo;
    }

    public void setLimcoins(int limcoins){
        this.limcoins = limcoins;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(pseudo);
        parcel.writeString(password);
        parcel.writeInt(limcoins);
    }

    public boolean equals(Object o){
        if(o instanceof Joueur){
            Joueur j = (Joueur)o;
            if(j.getPseudo().equals(this.pseudo) && j.getPassword().equals(this.password))
                return true;
        }
        return false;
    }
}
