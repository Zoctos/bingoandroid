package com.fabien.pierre.projetandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by Pierre on 18/12/2016.
 */

public class JoueurDAO extends DAOBase{

    public static final String TABLE_NAME = "Joueur";
    public static final String PSEUDO = "pseudo";
    public static final String PASSWORD = "password";

    public static final String JOUEUR_TABLE_CREATE = "CREATE TABLE "+TABLE_NAME+
            " ("+PSEUDO+" TEXT PRIMARY KEY,"+
            PASSWORD+" TEXT );";

    public JoueurDAO(Context mContext){
        super(mContext);
    }

    public Joueur getJoueur(String pseudo, String password){

        //Création de la requête
        String query = "SELECT "+PSEUDO+", "+PASSWORD+" FROM "+TABLE_NAME+" WHERE "+PSEUDO+" = ? AND PASSWORD = ?";

        //Exécution de la requête et passage des paramètres
        Cursor cursor = mDB.rawQuery(query, new String[]{pseudo, password});

        //Si on a trouvé un compte joueur correspondant
        if(cursor.getCount() == 1){

            //On le récupère et on le retourne
            cursor.moveToFirst();
            Joueur j = new Joueur(cursor.getString(0), cursor.getString(1));
            cursor.close();
            return j;

        }
        else {
            //Sinon on retourne null
            cursor.close();
            return null;
        }
    }

    public Joueur getJoueur(String pseudo){

        //Création de la requête
        String query = "SELECT "+PSEUDO+", "+PASSWORD+" FROM "+TABLE_NAME+" WHERE "+PSEUDO+" = ?";

        //Exécution de la requête
        Cursor cursor = mDB.rawQuery(query, new String[]{pseudo});

        //Si on a trouvé un compte joueur correspondant
        if(cursor.getCount() == 1){

            //On le récupère et on le retourne
            cursor.moveToFirst();
            Joueur j = new Joueur(cursor.getString(0), cursor.getString(1));
            cursor.close();
            return j;

        }
        else {
            //Sinon on retourne null
            cursor.close();
            return null;
        }

    }

}
