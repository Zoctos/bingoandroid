package com.fabien.pierre.projetandroid;

import android.os.Parcel;
import android.os.Parcelable;


public class Score implements Parcelable {
    private Joueur joueur;
    private int score;


    public Score(Joueur joueur, int score) {
        this.joueur = joueur;
        this.score = score;
    }

    protected Score(Parcel in) {
        joueur = in.readParcelable(Joueur.class.getClassLoader());
        score = in.readInt();
    }

    public static final Creator<Score> CREATOR = new Creator<Score>() {
        @Override
        public Score createFromParcel(Parcel in) {
            return new Score(in);
        }

        @Override
        public Score[] newArray(int size) {
            return new Score[size];
        }
    };

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void levelUp() {
        this.score ++;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(joueur, flags);
        dest.writeInt(score);
    }

    @Override
    public String toString() {
        return  joueur.getPseudo() + " : " + score;
    }
}
