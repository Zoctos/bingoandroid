package com.fabien.pierre.projetandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Pierre on 18/12/2016.
 */

public class ScoreDAO extends DAOBase {

    public static final String TABLE_NAME = "Score";

    private static final String ID = "id";
    private static final String JOUEUR = "joueurPseudo";
    private static final String LEVEL = "level";

    private JoueurDAO joueurDAO;

    public ScoreDAO(Context mContext){
        super(mContext);
        joueurDAO = new JoueurDAO(mContext);
    }

    public void addScore(Score score){

        ContentValues values = new ContentValues();
        values.put(JOUEUR, score.getJoueur().getPseudo());
        values.put(LEVEL, score.getScore());
        mDB.insert(TABLE_NAME, null, values);

    }

    public ArrayList<Score> getAllScore(){

        ArrayList<Score> scores = new ArrayList<Score>();

        String query = "SELECT "+JOUEUR+", "+LEVEL+" FROM "+TABLE_NAME+" ORDER BY "+LEVEL+" DESC ";

        Cursor cursor = mDB.rawQuery(query, new String[]{});

        joueurDAO.open();

        while(cursor.moveToNext()){

            String pseudo = cursor.getString(0);
            int level = cursor.getInt(1);

            Joueur j = joueurDAO.getJoueur(pseudo);

            if(j == null)
                continue;

            scores.add(new Score(j, level));
        }
        cursor.close();

        joueurDAO.close();

        return scores;
    }

}
